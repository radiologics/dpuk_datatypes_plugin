package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
  value = "dpukDatatypesPlugin",
  name = "DPUK Datatypes",
  description = "DPUK Datatypes plugin for XNAT 1.7 with DPUK Assessment and DPUK Risk Factors Assessment datatypes and DPUK Subject ID field added to subject.",
  dataModels = {
    @XnatDataModel(
      value = "dpuk:dpukAssessmentData",
      singular = "DPUK Assessment",
      plural = "DPUK Assessments"
    ),
    @XnatDataModel(
      value = "dpuk:dpukRiskFactorAssessmentData",
      singular = "DPUK Risk Factor",
      plural = "DPUK Risk Factors"
    )
  }
)
public class DpukDatatypesPlugin {
}
